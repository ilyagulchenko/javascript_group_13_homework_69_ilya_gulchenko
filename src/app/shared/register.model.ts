export class Register {
  constructor(
    public id: string,
    public comments: string,
    public name: string,
    public number: string,
    public patronymic: number,
    public placeOf: string,
    public size: number,
    public surname: string,
    public tShirt: number,
  ) {}
}
