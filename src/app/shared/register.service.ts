import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Register} from './register.model';
import {map, Subject} from 'rxjs';

@Injectable()

export class RegisterService {
  registersFetch = new Subject<Register[]>();

  constructor(private http: HttpClient) {}

  private registers: Register[] = [];

  addRegister(register: Register) {
    const body = {
      comments: register.comments,
      name: register.name,
      number: register.number,
      patronymic: register.patronymic,
      placeOf: register.placeOf,
      size: register.size,
      surname: register.surname,
      tShirt: register.tShirt,
    };

    return this.http.post('https://plovo-cc061-default-rtdb.firebaseio.com/register.json', body);
  }

  fetchRegister(id: string) {
    return this.http.get<Register>(`https://plovo-cc061-default-rtdb.firebaseio.com/register/${id}.json`).pipe(
      map(result => {
        return new Register(
          id,
          result.comments,
          result.name,
          result.number,
          result.patronymic,
          result.placeOf,
          result.size,
          result.surname,
          result.tShirt,
          );
      })
    )
  }

  fetchRegisters() {
    this.http.get<{[id: string]: Register}>('https://plovo-cc061-default-rtdb.firebaseio.com/register.json').pipe(
      map(result => {
        return Object.keys(result).map(id => {
          const registerData = result[id];

          return new Register(
            id,
            registerData.comments,
            registerData.name,
            registerData.number,
            registerData.patronymic,
            registerData.placeOf,
            registerData.size,
            registerData.surname,
            registerData.tShirt
          );
        });
      })
    ).subscribe(registers => {
      this.registers = registers;
      this.registersFetch.next(this.registers.slice());
    })
  }

  editRegister(register: Register) {
    const body = {
      comments: register.comments,
      name: register.name,
      number: register.number,
      patronymic: register.patronymic,
      placeOf: register.placeOf,
      size: register.size,
      surname: register.surname,
      tShirt: register.tShirt,
    }

    return this.http.put(`https://plovo-cc061-default-rtdb.firebaseio.com/register/${register.id}.json`, body);
  }

  removeRegister(id: string) {
    return this.http.delete(`https://plovo-cc061-default-rtdb.firebaseio.com/register/${id}.json`);
  }
}
