import {Component, Input} from '@angular/core';
import {Register} from '../shared/register.model';
import {RegisterService} from '../shared/register.service';

@Component({
  selector: 'app-register-item',
  templateUrl: './register-item.component.html',
  styleUrls: ['./register-item.component.css']
})
export class RegisterItemComponent {
  @Input() register!: Register;

  constructor(private registerService: RegisterService) {}

  onRemove() {
    this.registerService.removeRegister(this.register.id).subscribe(() => {
      this.registerService.fetchRegisters();
    });
  }
}
