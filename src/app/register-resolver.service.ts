import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Register} from './shared/register.model';
import {RegisterService} from './shared/register.service';
import {mergeMap, Observable, of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class RegisterResolverService implements Resolve<Register>{

  constructor(
    private router: Router,
    private registerService: RegisterService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Register> {
    const registerId = <string>route.params['id'];

    return this.registerService.fetchRegister(registerId).pipe(mergeMap(register => {
      return of(register);
    }))
  }
}
