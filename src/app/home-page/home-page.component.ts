import {Component, OnInit, ViewChild} from '@angular/core';
import { NgForm } from '@angular/forms';
import { RegisterService } from '../shared/register.service';
import { Register } from '../shared/register.model';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit{
  @ViewChild('f') userForm!: NgForm;
  role = '';

  isEdit = false;
  editedId = '';

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private registerService: RegisterService
  ) {}

  ngOnInit() {
    this.route.data.subscribe(data => {
      const register = <Register>data['register'];

      if (register) {
        this.isEdit = true;
        this.editedId = register.id;
        this.setFormValue({
          comments: register.comments,
          name: register.name,
          number: register.number,
          patronymic: register.patronymic,
          placeOf: register.placeOf,
          size: register.size,
          surname: register.surname,
          tShirt: register.tShirt,
        });
      } else {
        this.isEdit = false;
        this.editedId = '';
        this.setFormValue({
          comments: '',
          name: '',
          number: '',
          patronymic: '',
          placeOf: '',
          size: '',
          surname: '',
          tShirt: '',
        });
      }
    });
  }

  setFormValue(value: {[key: string] : any}) {
    setTimeout(() => {
      this.userForm.form.setValue(value);
    })
  }

  saveRegistration() {
    const id = this.editedId || Math.random().toString();
    const register = new Register(
      id,
      this.userForm.value.comments,
      this.userForm.value.name,
      this.userForm.value.number,
      this.userForm.value.patronymic,
      this.userForm.value.placeOf,
      this.userForm.value.size,
      this.userForm.value.surname,
      this.userForm.value.tShirt
    );

    const next = () => {
      void this.router.navigate(['**'], {relativeTo: this.route});
    };
    if (this.isEdit) {
      this.registerService.editRegister(register).subscribe();
    } else {
      this.registerService.addRegister(register).subscribe(next);
    }
  }
}
