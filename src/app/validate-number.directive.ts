import {AbstractControl, NG_VALIDATORS, ValidationErrors, Validator} from '@angular/forms';
import {Directive} from '@angular/core';

export const numberValidator = (control: AbstractControl): ValidationErrors | null => {
  const format = /^((\+996|996)+([0-9]){9})$/.test(control.value);

  if (format) {
    return null;
  }
  return {number: true};
};

@Directive({
  selector: '[appNumber]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: ValidateNumberDirective,
    multi: true
  }]
})

export class ValidateNumberDirective implements Validator {
  validate(control: AbstractControl): ValidationErrors | null {
    return numberValidator(control);
  }
}

