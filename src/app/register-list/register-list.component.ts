import {Component, OnDestroy, OnInit} from '@angular/core';
import {Register} from '../shared/register.model';
import {Subscription} from 'rxjs';
import {RegisterService} from '../shared/register.service';

@Component({
  selector: 'app-register-list',
  templateUrl: './register-list.component.html',
  styleUrls: ['./register-list.component.css']
})
export class RegisterListComponent implements OnInit, OnDestroy {
  registers!: Register[];
  registersChangeSubscription!: Subscription;

  constructor(private registerService: RegisterService) { }

  ngOnInit(): void {
    this.registersChangeSubscription = this.registerService.registersFetch.subscribe((registers: Register[]) => {
      this.registers = registers;
    });
    this.registerService.fetchRegisters();
  }

  ngOnDestroy(): void {
    this.registersChangeSubscription.unsubscribe();
  }
}
