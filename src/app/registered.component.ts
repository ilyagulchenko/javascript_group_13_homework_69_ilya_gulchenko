import {Component} from '@angular/core';

@Component({
  selector: 'app-registered',
  template: '<h1>Ваша регистрация принята, спасибо</h1>',
  styles: [`
    h1 {
      color: blue;
      margin-left: 15px;
    }
  `]
})
export class RegisteredComponent {}
