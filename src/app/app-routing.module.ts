import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisteredComponent } from './registered.component';
import { HomePageComponent } from './home-page/home-page.component';
import { RegisterListComponent } from './register-list/register-list.component';
import {RegisterResolverService} from './register-resolver.service';

const routes: Routes = [
  {path: '', component: HomePageComponent},
  {path: 'registers-list', component: RegisterListComponent},
  {path: 'edit/:id', component: HomePageComponent, resolve: {
    register: RegisterResolverService
    }},
  {path: '**', component: RegisteredComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
