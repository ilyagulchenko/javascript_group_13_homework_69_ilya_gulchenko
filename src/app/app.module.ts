import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { ValidateNumberDirective } from './validate-number.directive';
import { AppRoutingModule } from './app-routing.module';
import { RegisteredComponent } from './registered.component';
import { HomePageComponent } from './home-page/home-page.component';
import {RegisterService} from './shared/register.service';
import {HttpClientModule} from '@angular/common/http';
import { RegisterListComponent } from './register-list/register-list.component';
import { RegisterItemComponent } from './register-item/register-item.component';

@NgModule({
  declarations: [
    AppComponent,
    ValidateNumberDirective,
    RegisteredComponent,
    HomePageComponent,
    RegisterListComponent,
    RegisterItemComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [RegisterService],
  bootstrap: [AppComponent]
})
export class AppModule { }
